package com.example.springhomework02.service;

import com.example.springhomework02.model.Customer;
import com.example.springhomework02.respository.CustomerRespository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerImplement implements CustomerService {
    private final CustomerRespository customerRespository;

    public CustomerImplement(CustomerRespository customerRespository) {
        this.customerRespository = customerRespository;
    }

    @Override
    public List<Customer> customer() {
        return null;
    }

    @Override
    public List<Customer> getCustomerById(Integer id) {
        customerRespository.getCustomerById (id);
        return null;
    }


    @Override
    public List<Customer> updateCustomerById(Integer id, Customer customerId) {
        customerRespository.updateCustomerById (id,customerId);
        return null;
    }

    @Override
    public List<Customer> deleteCustomerById(Integer id) {
        customerRespository.deleteCustomerById (id);
        return null;
    }
}