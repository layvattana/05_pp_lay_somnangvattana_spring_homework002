package com.example.springhomework02.service;

import com.example.springhomework02.model.Customer;

import java.util.List;

public interface CustomerService {




    List<Customer> customer();
    List<Customer> getCustomerById(Integer id);
    List<Customer> updateCustomerById(Integer id, Customer customerId);
    List<Customer> deleteCustomerById(Integer id);



}