package com.example.springhomework02.service;

import com.example.springhomework02.model.Product;
import com.example.springhomework02.respository.ProductRespository;
import org.springframework.stereotype.Service;
import java.util.List;


@Service

public class  ProductImplement implements ProductService {
    private final ProductRespository productRespository;

    public ProductImplement(ProductRespository productRespository) {
        this.productRespository = productRespository;
    }


    @Override
    public List<Product> product() {
        return productRespository.ProductSelect();
    }

    @Override
    public List<Product> getProductById(Integer productId) {
        return productRespository.getProductById(productId);
    }

    @Override
    public List<Product> updateProductById(Integer id, Product productId) {
       productRespository.updateProduct (id,productId);

        return null;
    }
    @Override
    public List<Product> deleteProductById(Integer id) {
        productRespository.deleteProductById (id);
        return null;
    }


}