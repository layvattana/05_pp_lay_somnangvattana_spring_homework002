package com.example.springhomework02.service;
import com.example.springhomework02.model.Product;


import java.util.List;


public interface ProductService {

    List<Product> product();

    List<Product> getProductById(Integer productId);

    List<Product> updateProductById(Integer id, Product productId);
    List<Product> deleteProductById(Integer id);

}
