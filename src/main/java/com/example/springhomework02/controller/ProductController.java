package com.example.springhomework02.controller;

import com.example.springhomework02.model.Customer;
import com.example.springhomework02.model.Product;
import com.example.springhomework02.model.request.ProductRequest;
import com.example.springhomework02.model.response.CustomerResponse;
import com.example.springhomework02.model.response.ProductResponse;
import com.example.springhomework02.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/api/v1/products")

public class ProductController {

    private final ProductService productService;
@Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    //    Show all
    @GetMapping("/Showall")
    @Operation(summary = "Show All Product")
    public ResponseEntity<ProductResponse<List<Product>>> showAll() {
        ProductResponse<List<Product>> responseEntity = ProductResponse.<List<Product>>builder ()
                .payload (productService.product ())
                .Message ("Get all data")
                .Successful (true)
                .build ();
        return ResponseEntity.ok (responseEntity);
    }
    // Get by ID
    @GetMapping("/productByid/{id}")
    @Operation(summary = "Get by ID Product")

    public ResponseEntity<?> getProductById(@PathVariable Integer id ) {
        List<Product> product = productService.getProductById (id);
        ProductResponse<List<Product>> responseEntity = ProductResponse.<List<Product>>builder ()
                .payload (productService.product ())
                .Message ("Fetch data ")
                .Successful (true)
                .build ();

        return ResponseEntity.ok (responseEntity);
    }
        // Update by ID
        @Operation(summary = "Update ID")

        @PutMapping("/update-productbyid/{id}")
        public ResponseEntity<?> updateProductById(@PathVariable Integer id, @RequestBody Product productId) {
            List<Product> product = productService.updateProductById (id,productId);
            ProductResponse<List<Product>> responseEntity = ProductResponse.<List<Product>>builder ()
                    .payload (productService.product ())
                    .Message ("Get data updated")
                    .Successful (true)
                    .build ();

                 return ResponseEntity.ok (responseEntity);

        }
        // Delete by ID
        @Operation(summary = "Delete by ID Product")

        @DeleteMapping("delete-customerbyid/{id}")
        public ResponseEntity<?> deleteProductById(@PathVariable Integer id){
                ProductResponse<List<Product>> responseEntity = ProductResponse.<List<Product>>builder ()
                .payload (productService.deleteProductById (id))
                .Message ("Get data deleted")
                .Successful (true)
                .build ();
                return ResponseEntity.ok (responseEntity);

        }
    }

