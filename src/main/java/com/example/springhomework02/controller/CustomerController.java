package com.example.springhomework02.controller;

import com.example.springhomework02.model.ApiResponse;
import com.example.springhomework02.model.Customer;
import com.example.springhomework02.model.response.CustomerResponse;
import com.example.springhomework02.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.security.Timestamp;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")

public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;

    }

    //    Show all
    @GetMapping("/Showall")
    @Operation(summary = "Show All Customer")
    public ResponseEntity<CustomerResponse<List<Customer>>> showAll() {
        CustomerResponse<List<Customer>> responseEntity = CustomerResponse.<List<Customer>>builder ()
                .payload (customerService.customer ())
                .Message ("Get all data")
                .Successful (true)
                .build ();
        return ResponseEntity.ok (responseEntity);
    }

    // Get by ID
    @GetMapping("/customerById/{id}")
    @Operation(summary = "Get by ID Customer")

    public ResponseEntity<?> getCustomerById(@PathVariable Integer id ) {
        List<Customer> customer = customerService.getCustomerById(id);
        CustomerResponse<List<Customer>> responseEntity = CustomerResponse.<List<Customer>>builder ()
                .payload (customerService.customer ())
                .Message ("Fetch data ")
                .Successful (true)
                .build ();

        return ResponseEntity.ok (responseEntity);
    }
    // Update by ID
    @PutMapping("/update-customerById/{id}")
    @Operation(summary = "Update by ID Customer")
    public ResponseEntity<?> updateCustomerById(@PathVariable Integer id, @RequestBody Customer customerId) {
        List<Customer> customer = customerService.updateCustomerById (id,customerId);
        CustomerResponse<List<Customer>> responseEntity = CustomerResponse.<List<Customer>>builder ()
                .payload (customerService.customer ())
                .Message ("Get data updated")
                .Successful (true)
                .build ();

        return ResponseEntity.ok (responseEntity);

    }
    // Delete by ID
    @DeleteMapping("delete-customerById/{id}")
    @Operation(summary = "Delete by ID Customer")

    public ResponseEntity<?> deleteCustomerById(@PathVariable Integer id){
        CustomerResponse<List<Customer>> responseEntity = CustomerResponse.<List<Customer>>builder ()
                .payload (customerService.deleteCustomerById (id))
                .Message ("Get data delete")
                .Successful (true)
                .build ();
        return ResponseEntity.ok (responseEntity);

    }
}

