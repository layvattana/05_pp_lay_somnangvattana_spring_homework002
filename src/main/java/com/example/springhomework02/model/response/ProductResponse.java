package com.example.springhomework02.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class ProductResponse<C> {
    private C payload;
    private String Message;

    private Boolean Successful;

}