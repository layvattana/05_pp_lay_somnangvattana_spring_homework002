package com.example.springhomework02.model;

public class Invoice {
    public Invoice(String invoiceDate, Integer invoiceId) {
        this.invoiceDate = invoiceDate;
        this.invoiceId = invoiceId;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Invoice() {
    }

    private String invoiceDate;
    private Integer invoiceId;
}
