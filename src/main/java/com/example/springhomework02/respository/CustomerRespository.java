package com.example.springhomework02.respository;

import com.example.springhomework02.model.Customer;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRespository {
    // Show All
    @Select("SELECT * FROM customers")
    @Result(property = "customerId", column = "customer_id")
    @Result(property = "customerName", column = "customer_name")
    @Result(property = "customerAddress", column = "customer_address")
    @Result(property = "customerPhone", column = "customer_phone")
    List<Customer> customerSelect();



    // Get By ID
    @Select("  SELECT * FROM customers WHERE customer_id = #{id};")
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerPrice",column = "customer_price")
    List<Customer> getCustomerById(Integer Id);


    // Update by ID

    @Update("""
            UPDATE customers SET customer_name = #{customers.customerName} , customer_price = #{customers.customerPrice} WHERE customer_id = #{Id};
            """)
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerPrice",column = "customer_price")
    void updateCustomerById(Integer id, Customer customerId);
    // Delete

    @Delete("""
            DELETE From customers WHERE customer_id = #{pcustomerId};
            """)
    void deleteCustomerById(Integer customerId);



}



