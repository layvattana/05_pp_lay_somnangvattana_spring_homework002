package com.example.springhomework02.respository;

import com.example.springhomework02.model.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper

public interface ProductRespository {

    // Show all
    @Select("SELECT * FROM products")
    @Result(property = "productId", column = "product_id")
    @Result(property = "productName", column = "product_name")
    @Result(property = "productPrice", column = "product_price")
    List<Product> ProductSelect();

    // Get By ID
    @Select("  SELECT * FROM products WHERE product_id = #{id};")
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "product_name")
    @Result(property = "productPrice",column = "product_price")
    List<Product> getProductById(Integer Id);


    // Update by ID

    @Update("""
            UPDATE products SET product_name = #{products.productName} , product_price = #{products.productPrice} WHERE product_id = #{Id};
            """)
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "product_name")
    @Result(property = "productPrice",column = "product_price")
    void updateProduct(Integer Id ,@Param("products") Product productRequest);

    // Delete

    @Delete("""
            DELETE From products WHERE product_id = #{productId};
            """)
    void deleteProductById(Integer productId);
}
