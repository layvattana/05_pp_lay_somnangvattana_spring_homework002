CREATE SCHEMA IF NOT EXISTS public;
grant usage on schema public to public;
grant create on schema public to public;

-- CREATE CUSTOMER
CREATE TABLE customers(
                          customer_id SERIAL PRIMARY KEY NOT NULL,
                          customer_name varchar(255) NOT NULL ,
                          customer_address varchar(255) NOT NULL ,
                          customer_phone int NOT NULL
);
-- CREATE PRODUCT
CREATE TABLE products(
                         product_id SERIAL PRIMARY KEY NOT NULL,
                         product_name varchar(255) NOT NULL ,
                         product_price int NOT NULL
);
-- CREATE INVOICE
CREATE TABLE Invoice(
                        invoice_id Serial PRIMARY KEY NOT NULL,
                        invoice_date DATE,
                        customer_id INT REFERENCES customers(customer_id)

);
-- CREATE INVOICE-detail
CREATE TABLE Invoice_detail(
                               id Serial PRIMARY KEY NOT NULL,
                               invoice_id int NOT NULL ,
                               product_id int REFERENCES products(product_id)

);

-- INSERT INTO CUSTOMER
INSERT INTO public.customers
    (customer_id, customer_name,customer_address, customer_phone)
        VALUES (1, 'Vattana','PP', '012406645');
INSERT INTO public.customers
    (customer_id, customer_name,customer_address, customer_phone)
        VALUES (2, 'Sokreach','PP' ,'012253063');
INSERT INTO public.customers
    (customer_id, customer_name,customer_address, customer_phone)
        VALUES (3, 'Vita','PP' ,'093438188');




INSERT INTO public.products
    (product_id, product_name, product_price)
        VALUES (1, 'Logitech G502 X Wired',69.99);
INSERT INTO public.products
    (product_id, product_name, product_price)
        VALUES (2, 'Logitech G Pro X Super Light', 144.99);
INSERT INTO public.products
    (product_id, product_name, product_price)
        VALUES (3, 'Logitech G Pro X Wireless', 239.99);
INSERT INTO public.products
    (product_id, product_name, product_price)
        VALUES (4, 'SteelSeries Arctis Nova Pro', 279.99);
INSERT INTO public.products
    (product_id, product_name, product_price)
        VALUES (5, 'Razer Pro Type', 139.99);
INSERT INTO public.products
    (product_id, product_name, product_price)
        VALUES (6, 'Logitech Pop Keys' , 99.99);

--
-- -- JOIN RELATIONSHIP
-- SELECT product_id
-- FROM products , Invoice_detail , customers , Invoice
-- WHERE products.product_id = invoice_detail.product_id
-- AND products.product_id >= Invoice_detail.product_id LIMIT 10;









